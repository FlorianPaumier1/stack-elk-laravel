<?php


namespace App\Http\Controllers;


use Cviebrock\LaravelElasticsearch\Manager;
use Illuminate\Routing\Controller as BaseController;

class HomeController extends BaseController
{

    private $elkManager;

    private $params = [
        'index' => 'first_group',
        'body' => [
            'query' => []
        ]
    ];

    public function __construct(Manager $elkManager)
    {
        $this->elkManager = $elkManager->connection();
    }

    /**
     * Question 2
     * @return array|callable
     */
    public function count()
    {
        $this->params["body"]["query"] = ['match_all' => new \stdClass()];
        return $this->elkManager->search($this->params);
    }

    /**
     * Question 3
     * @return array|callable
     */
    public function length()
    {
        $this->params["body"]["query"] = ['match_all' => new \stdClass()];
        $this->params["size"] = 100;
        return $this->elkManager->search($this->params);
    }

    /**
     * Question 4
     * @return array|callable
     */
    public function date()
    {
        $this->params["body"]["query"] = ["match" => [
            "date" => [
                "query" => "may 2017",
                "operator" => "and"
            ]]];
        return $this->elkManager->search($this->params);
    }

    /**
     * Question 5
     * @return array|callable
     */
    public
    function elastic()
    {

        $this->params["body"]["query"] = ['match' => ["title" => "elastic"]];
        return $this->elkManager->search($this->params);
    }

    /**
     * Question 6
     * @return array|callable
     */
    public
    function elasticStack()
    {
        "Par défaut c'est un opérateur OR";
        $this->params["body"]["query"] = ['match' => ["title" => "elastic stack"]];
        return $this->elkManager->search($this->params);
    }

    /**
     * Question 7
     * @return array|callable
     */
    public
    function elasticStackAnd()
    {
        $this->params["body"]["query"] = ['match' => ["title" => ["query" => "elastic stack", "operator" => "and"]]];
        return $this->elkManager->search($this->params);
    }


    /**
     * Question 8
     * @return array|callable
     */
    public function search()
    {
        //8.1
        $this->params["body"]["query"] = ['match' => ["content" => "search"]];
        //8.2
        $this->params["body"]["query"] = ['match' => ["content" => "search analytics"]];
        //8.3
        $this->params["body"]["query"] = ['match' => ["content" => ["query" => "search analytics", "operator" => "and"]]];
        return $this->elkManager->search($this->params);
    }


    /**
     * Question 9
     * @return array|callable
     */
    public
    function matchPhrase()
    {
        $this->params["body"]["query"] = ['match_phrase' => ["content" => "search analytics"]];
        return $this->elkManager->search($this->params);
    }

    /**
     * Question 10
     * @return array|callable
     */
    public
    function matchPhraseRegex()
    {
        $this->params["body"]["query"] = ['regexp' => ["content" => ["value" => "search .*+ analytics"]]];
        return $this->elkManager->search($this->params);
    }

    /**
     * Question 11
     * @return array|callable
     */
    public
    function question()
    {
        $this->params["body"]["query"] = ['match' => ["content" => ["query" => "performance optimizations improvements", "operator" => "or"]]];
        return $this->elkManager->search($this->params);
    }

    /**
     * Question 12
     */
    public function bool()
    {

        $this->params["body"]["query"] = ["bool" => [
            "should" => [
                [
                    "match" => [
                        "content" => "performance"
                    ]
                ],
                [
                    "match" => [
                        "content" => "optimizations"
                    ]
                ],
                [
                    "match" => [
                        "content" => "improvements"
                    ]
                ]
            ],
            "minimum_should_match" => 2
        ]];
        return $this->elkManager->search($this->params);
    }

    /**
     * Question 13
     * @return array|callable
     */
    public
    function scoreMax()
    {
        return ["scoreMax" => 9.979115, "problème" => "Ce sont des articles et non des aides donc faire une recherche plus précise ou mettre l'opérateur 'and'"];
    }
}
