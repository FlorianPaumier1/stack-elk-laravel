<?php

namespace App\Console\Commands;

use Cviebrock\LaravelElasticsearch\Manager;
use Illuminate\Console\Command;

class InsertDataElk extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'elk:insert';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';
    /**
     * @var \Cviebrock\LaravelElasticsearch\Manager
     */
    private $elkManager;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct(Manager $elkManager)
    {
        $this->elkManager = $elkManager;
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $file = file_get_contents(dirname(__DIR__, 3) . "/storage/app/Data.csv");

        $rows = explode("\n", $file);

        $connection = $this->elkManager->connection();

        $params = ["body" => []];

        foreach ($rows as $key => $row) {
            $values = explode(";", $row);
            if (count($values) < 2) continue;

            $params['body'][] = [
                'index' => [
                    '_index' => 'first_group',
                    '_id' => $key
                ]
            ];

            $params['body'][] = [
                "title" => $values[0],
                "seo_title" => $values[1],
                "url" => $values[2],
                "author" => $values[3],
                "date" => $values[4],
                "category" => $values[5],
                "locales" => $values[6],
                "content" => $values[7],
            ];

            // Every 1000 documents stop and send the bulk request
            if ($key % 1000 == 0) {
                $responses = $connection->bulk($params);

                // erase the old bulk request
                $params = ['body' => []];

                // unset the bulk response when you are done to save memory
                unset($responses);
            }
        }

        // Send the last batch if it exists
        if (!empty($params['body'])) {
            $responses = $connection->bulk($params);
        }
        return 0;
    }
}
