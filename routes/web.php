<?php

use App\Http\Controllers\HomeController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/2',[HomeController::class, 'count']);
Route::get('/3',[HomeController::class, 'length']);
Route::get('/4',[HomeController::class, 'date']);
Route::get('/5',[HomeController::class, 'elastic']);
Route::get('/6',[HomeController::class, 'elasticStack']);
Route::get('/7',[HomeController::class, 'elasticStackAnd']);
Route::get('/8',[HomeController::class, 'search']);
Route::get('/9',[HomeController::class, 'matchPhrase']);
Route::get('/10',[HomeController::class, 'matchPhraseRegex']);
Route::get('/11',[HomeController::class, 'question']);
Route::get('/12',[HomeController::class, 'bool']);
Route::get('/13',[HomeController::class, 'scoreMax']);
